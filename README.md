<h1>Resources</h1>

## Summary
This project has links and snippets for resources needed throughout the **Go Forth and Rock** E-Learning course


## Snippets
You can access course Snippets [here](https://gitlab.com/goforthandrock/resources/snippets)